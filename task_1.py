# Написати програму, яка визначає якою є число - парним чи непарним. Якщо залишок від розподілу на 2 не
# дорівнює 0, це непарне число, а якщо дорівнює - то парне.

numeric_str = input("введіть число: ")
parts = numeric_str.partition(".")
if (parts[0].isdigit() and parts[2].isdigit()) or (not parts[1] and parts[0].isdigit()):
    numeric = float(numeric_str)
    if numeric % 2:
        print("число непарне")
    else:
        print("число парне")
else:
    print("число складається з цифр і може включати одну крапку як роздільник цілої і дробової частини")
